#!/usr/bin/env bash

# Check that the application can be served successfully.

flask run &
sleep 1
curl http://127.0.0.1:5000/status
pkill flask
