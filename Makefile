help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  lint             to check the source for stylistic errors."
	@echo "  clean            to remove all cache files."
	@echo "  run              to run the Flask server."
	@echo "  install          to install the project dependencies."
	@echo "  test             to run the tests."
	@echo "  isort            to check the source for disorganised imports."
	@echo "  ci               to imitate a Gitlab CI build run."
	@echo "  devserver        to serve the application for development."
	@echo "  prodserver       to serve the application for production."
	@echo "  checkserver      to check if the application server comes up."
.PHONY: help

lint:
	pylama oidcp/ test/
.PHONY: lint

clean:
	find . -name "__pycache__" | xargs rm -rf
.PHONY: clean

install:
	pip install -U -e . -r deps/all.txt
.PHONY: install

test:
	py.test -q test/
.PHONY: test

isort:
	find oidcp -name "*.py" | xargs isort -c
.PHONY: isort

coverage:
	py.test --cov=oidcp test
.PHONY: coverage

ci: lint isort test
.PHONY: ci

devserver:
	flask run
.PHONY: devserver

prodserver:
	gunicorn wsgi:app
.PHONY: prodserver

checkserver:
	./checkserver.sh
.PHONY: checkserver
