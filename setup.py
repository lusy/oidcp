from setuptools import find_packages, setup

setup(
    name='oidcp',
    version='0.0.1',
    description='OIDC Provider for the FSFE',
    url='http://gitlab.com/lukewm/oidcp',
    author='ams-hackers',
    author_email='ams-hackers@q.fsfe.org',
    license='AGPL',
    packages=find_packages("."),
    zip_safe=False,
    install_requires=[
        'oic',
        'Flask',
    ]
)
