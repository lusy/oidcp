# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""Provider view routing."""

from flask import Blueprint, current_app, jsonify, request

from oic.utils.http_util import extract_from_request

op_blueprint = Blueprint('op', __name__)


@op_blueprint.route('/auth', methods=['GET', 'POST'])
def auth():
    """The authorization endpoint.

    This endpoint handles:

      * End-User authentication and authorization.
      * Generating intermediate codes for the Client.

    """
    env = extract_from_request(request.environ)
    env.update({'cookie': request.environ.get('HTTP_SET_COOKIE')})
    return current_app.provider.authorization_endpoint(**env)


@op_blueprint.route('/token', methods=['POST'])
def token():
    """The token endpoint.

    This endpoint handles:

      * Issue access, identification and refresh tokens.

    """
    env = extract_from_request(request.environ)
    return current_app.provider.token_endpoint(**env)


@op_blueprint.route('/verify', methods=['POST'])
def verify():
    """The verify endpoint.

    This endpoint handles:

      * Accepting End-User credentials.

    """
    env = extract_from_request(request.environ)
    return current_app.provider.verify_endpoint(**env)


@op_blueprint.route('/introspect', methods=['POST'])
def introspect():
    """The introspection endpoint.

    This endpoint handles:

      * Validating that a JWT identitation token is valid.

    """
    env = extract_from_request(request.environ)
    env.update(dict(headers=request.headers))
    return current_app.provider.introspection_endpoint(**env)


@op_blueprint.route('/status', methods=['GET'])
def status():
    """The status endpoint.

    This endpoint handles:

      * Exposing a simple check for API status.
        * In Irish. Time to learn people.

    """
    return jsonify({'stádas': 'Ta me réidh!'})
