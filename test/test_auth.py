# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""Authorization endpoint testing."""

from datetime import datetime as dt
from datetime import timedelta
from urllib.parse import urlencode

import pytest
from flask import current_app, url_for
from freezegun import freeze_time
from werkzeug.datastructures import Headers

from http.cookies import SimpleCookie


def test_auth_get(client, auth_params):
    url = url_for('op.auth')
    auth_params.update(prompt='login', acr_values='LDAP')
    resp = client.get(url, query_string=urlencode(auth_params))
    assert resp.status_code == 200
    assert resp.mimetype == 'text/html'
    assert url_for('op.verify', _external=True) in str(resp.data)


def test_auth_post_with_cookie(app, client, auth_params):
    redis = app.provider.sdb._db

    url = url_for('op.auth')

    state = auth_params['state']
    cookie_header = current_app.provider.cookie_func(state, "uam")
    cookie_value = SimpleCookie(cookie_header[1])
    redis[state] = cookie_value

    data = urlencode(auth_params)
    headers = Headers({cookie_header[0]: cookie_header[1]})
    resp = client.get(url, query_string=data, headers=headers)

    assert resp.status_code == 303
    assert resp.mimetype == 'text/html'
    assert "code=" in str(resp.data)
    assert auth_params['state'] in str(resp.data)

    assert len(redis.keys()) == 1

    session_key = str(redis.keys()[0], 'utf-8')
    session = redis[session_key]
    assert session['code_used'] is False
    assert session['revoked'] is False
    assert session['client_id'] == app.config['FRONTEND_ID']
    assert session['nonce'] == auth_params['nonce']
    assert session['state'] == auth_params['state']


def test_auth_post_with_missing_cookie(client, auth_params):
    url = url_for('op.auth')

    state = auth_params['state']
    cookie_header = current_app.provider.cookie_func(state, "uam")

    data = urlencode(auth_params)
    headers = Headers({cookie_header[0]: cookie_header[1]})
    resp = client.get(url, query_string=data, headers=headers)

    assert resp.status_code == 200
    assert "code=" not in str(resp.data)


def test_auth_post_with_cookie_too_late(client, auth_params):
    cookie = current_app.provider.cookie_func(auth_params['state'], "uam")
    url = url_for('op.auth')
    headers = Headers({cookie[0]: cookie[1]})
    too_late = dt.now() + timedelta(minutes=10)
    with freeze_time(too_late):
        resp = client.post(url, data=auth_params, headers=headers)
        assert resp.status_code == 200
        assert resp.mimetype == 'text/html'


@pytest.mark.parametrize('params', (
    dict(response_type='r', client_id='c', scope='s'),
    dict(client_id='c', scope='s', redirect_uri='r'),
    dict(response_type='r', scope='s', redirect_uri='r'),
    dict(response_type='r', client_id='c', redirect_uri='r'),
))
def test_auth_missing_params(client, params):
    url = url_for('op.auth')
    resp = client.get(url, query_string=urlencode(params))
    assert resp.status_code == 400
    assert resp.mimetype == 'application/json'
