# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""Token endpoint testing."""

import jwt
from flask import url_for


def test_token_post(app, client, token_params, session, auth_params):
    token_params.update({'code': session['code']})
    url = url_for("op.token")
    resp = client.post(url, data=token_params)
    assert resp.status_code == 200
    assert resp.mimetype == "application/json"

    actual_keys = resp.json.keys()
    expected_keys = [
        "access_token", "id_token",
        "token_type", "scope", "state"
    ]
    assert set(actual_keys) == set(expected_keys)

    assert resp.json['scope'] == "openid"
    assert resp.json['token_type'] == "Bearer"
    assert resp.json['state'] == "some_state"

    client_id = app.config['FRONTEND_ID']
    secret = app.config['CLIENT'][client_id]['client_secret']
    alg = app.config['CLIENT'][client_id]['id_token_signed_response_alg']

    token = resp.json['id_token']
    decoded = jwt.decode(
        token, secret, algorithms=alg,
        audience=app.config['FRONTEND_ID']
    )

    assert decoded['iss'] == app.config['ISSUER']
    assert decoded['nonce'] == auth_params['nonce']
    assert decoded['aud'] == [auth_params['client_id']]

    sid = app.provider.sdb.uid2sid[auth_params['state']]
    assert len(sid) == 1
    session = app.provider.sdb[sid[0]]
    assert session['code_used']


def test_token_post_missing_code(client, token_params):
    url = url_for("op.token")
    resp = client.post(url, data=token_params)
    assert resp.status_code == 400
    assert resp.json['error_description'] == "Missing code"


def test_token_post_code_reuse(client, token_params, session):
    token_params.update({'code': session['code']})
    url = url_for("op.token")
    resp = client.post(url, data=token_params)
    assert resp.status_code == 200
    assert resp.mimetype == "application/json"

    resp = client.post(url, data=token_params)
    assert resp.status_code == 400


def test_token_post_invalid_code(client, token_params):
    token_params.update({'code': 'INVALID'})
    url = url_for("op.token")
    resp = client.post(url, data=token_params)
    assert resp.status_code == 400
    assert resp.json['error_description'] == "Code is invalid"


def test_token_bad_http_method(client):
    url = url_for("op.token")
    resp = client.get(url)
    assert resp.status_code == 405
    assert resp.mimetype == "text/html"
