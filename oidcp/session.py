# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""Session management interface module."""

import json
import pickle

from oic.utils.sdb import SessionDB as PyOIDCSessionDB


class SessionDB(PyOIDCSessionDB):
    def revoke_token(self, token):
        """Revoke a token."""
        _, sid = self._get_token_type_and_key(token)
        session = self._db[sid]
        session['revoked'] = True
        self._db[sid] = session


class RedisSessionDB(dict):
    """Dictionary like interface for Redis session management.

    We handle complex objects by pickling them. This isn't dangerous
    because only the server application (trusted!) will pickle objects.
    """
    def __init__(self, conn, *args, **kwargs):
        self.conn = conn

    def __repr__(self):
        show = {}
        keys = self.conn.keys()
        utf8ified = map(lambda key: str(key, 'utf-8'), keys)
        for key in utf8ified:
            show[key] = self[key]
        return json.dumps(show)

    def __getitem__(self, key):
        pickled = self.conn.get(key)
        if pickled is None:
            raise KeyError()
        return pickle.loads(pickled)

    def __setitem__(self, key, val):
        pickled = pickle.dumps(val)
        self.conn.set(key, pickled)

    def __contains__(self, item):
        return self.conn.exists(item)

    def __delitem__(self, key):
        self.conn.delete(key)
