# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""HTTP helpers."""

from oic.utils.http_util import Response


class Redirect(Response):
    _status = '302 Found'
    _content_type = 'text/html'

    def __init__(self, redirect=None, **kwargs):
        super().__init__(redirect, **kwargs)

    def _response(self, message="", **kwargs):
        return self.template.render()
