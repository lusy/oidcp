# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""The PyOIDC provider module."""

import binascii
from urllib.parse import parse_qs

import jwt
from flask import jsonify

from oic.oic.provider import Provider as PYOIDCProvider
from oic.utils.authn.client import verify_client
from oic.utils.http_util import Response
from oidcp.auth import requires_basic_auth


class Provider(PYOIDCProvider):
    @requires_basic_auth
    def introspection_endpoint(self, request=None, headers=None, **kwargs):
        """Validate an access token."""
        data = parse_qs(request)

        try:
            ttype = data['token_type_hint'][0]
            code = data['access_token'][0]
        except KeyError as key_err:
            return Response(status="400 Bad Request")

        try:
            validator = self.sdb.token_factory[ttype]
            active = validator.valid(code)
        except binascii.Error:
            return Response(status="400 Bad Request")

        client_id = self.sdb[code]['client_id']
        alg = self.cdb[client_id]['id_token_signed_response_alg']
        secret = self.cdb[client_id]['client_secret']
        token = self.sdb[code]['id_token']
        try:
            jwt.decode(token, secret, algorithms=alg, audience=client_id)
        except jwt.exceptions.ExpiredSignatureError:
            active = False

        still_valid = active and not self.sdb.is_revoked(code)
        return jsonify({'active': still_valid})


def configure_provider(app):
    """Configure a provider with the application configuration.

    :param app: The application context.

    """
    issuer = app.config['ISSUER']
    session_db = app.config['SESSION']
    client_db = app.config['CLIENT']
    authz_handler = app.config['AUTHZ']

    auth_broker = None
    user_info_store = None
    sym_key = ''

    verification_function = verify_client

    return Provider(
        issuer,
        session_db,
        client_db,
        auth_broker,
        user_info_store,
        authz_handler,
        verification_function,
        sym_key,
    )
