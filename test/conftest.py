# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""Pytest fixtures for testing."""

from base64 import b64encode

import pytest
from werkzeug.datastructures import Headers

from oic.oic.message import AuthorizationRequest
from oic.utils.sdb import AuthnEvent
from oidcp.app import create_app
from oidcp.config import Test


@pytest.fixture
def ldap_mock(app):
    """A mocked LDAP connection."""
    conn = app.config['LDAP_CONN']
    yield conn.start()
    conn.stop()


@pytest.fixture
def app():
    """A testing application."""
    _app = create_app(Test)
    ctx = _app.test_request_context()
    ctx.push()
    yield _app
    ctx.pop()


@pytest.fixture
def auth_params(app):
    """All mandatory parameters for an authentication request."""
    return dict(
        response_type=app.config['FLOW'],
        client_id=app.config['FRONTEND_ID'],
        redirect_uri=app.config['FRONTEND_URI'],
        scope=app.config['SCOPE'],
        state='some_state',
        nonce='some_nonce'
    )


@pytest.fixture
def token_params(app):
    """All mandatory parameters for an ID token request."""
    client_id = app.config['FRONTEND_ID']
    secret = app.config['CLIENT'][client_id]['client_secret']
    return dict(
        grant_type='authorization_code',
        redirect_uri=app.config['FRONTEND_URI'],
        client_id=app.config['FRONTEND_ID'],
        client_secret=secret
    )


@pytest.fixture
def session(app, auth_params):
    """A mock session. PyOIDC uses this to manage state."""
    session_db = app.provider.sdb
    authreq = AuthorizationRequest(**auth_params)
    sid = session_db.access_token.key(user=auth_params['state'], areq=authreq)
    access_grant = session_db.access_token(sid=sid)

    client_id = app.config['FRONTEND_ID']
    client_salt = app.config['CLIENT'][client_id]['client_salt']
    auth_event = AuthnEvent(auth_params['state'], client_salt)

    session = {
        "authn_event": auth_event,
        "authzreq": authreq.to_json(),
        "client_id": client_id,
        "code": access_grant,
        "code_used": False,
        "scope": [app.config['SCOPE']],
        "redirect_uri": app.config['FRONTEND_URI'],
        "nonce": "some_nonce",
        "state": "some_state",
    }

    session_db[sid] = session
    session_db.do_sub(sid, client_id)
    session_db.uid2sid = {auth_params['state']: [sid]}

    return session


@pytest.fixture
def auth_header(app):
    """Basic authentication header from back-end."""
    client_id = app.config['BACKEND_ID']
    secret = app.config['BACKEND_SECRET']
    unencoded = "{}:{}".format(client_id, secret)
    encoded = b64encode(bytes(unencoded, "utf-8")).decode("ascii")
    return Headers({'Basic': encoded})
