import os
import sys

sys.path.insert(0, os.path.abspath('oidcp'))

extensions = [
    'sphinxcontrib.httpdomain',
    'sphinxcontrib.autohttp.flask',
]

templates_path = ['_templates']

source_suffix = '.rst'

master_doc = 'index'

project = 'oidcp'

copyright = '2017, ams-hackers'

author = 'ams-hackers'

version = '0.0.1'

release = '0.0.1'

exclude_patterns = ['_build']

pygments_style = 'sphinx'

todo_include_todos = True

html_theme = 'alabaster'

html_static_path = ['_static']

htmlhelp_basename = 'oidcpdoc'

latex_elements = {}

latex_documents = [(
    master_doc, 'oidcp.tex', 'oidcp Documentation',
    'ams-hackers', 'manual'
)]

man_pages = [(
    master_doc, 'oidcp',
    'oidcp Documentation', [author], 1
)]

texinfo_documents = [(
    master_doc, 'oidcp', 'oidcp Documentation',
    author, 'oidcp', 'One line description of project.',
    'Miscellaneous'
)]
