# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""The LDAP authentication module."""

import time
from base64 import b64decode
from binascii import Error
from functools import wraps
from http.cookies import SimpleCookie
from urllib.parse import parse_qs, urlencode

from flask import current_app, url_for
from jinja2 import Environment, PackageLoader, select_autoescape
from ldap import INVALID_CREDENTIALS
from oic.utils.authn.authn_context import AuthnBroker
from oic.utils.authn.user import ToOld, UserAuthnMethod
from oic.utils.http_util import Response
from oidcp.utils.http import Redirect


def requires_basic_auth(f):
    """HTTP Basic authentication verifier.

    Only the back-end is designated to make requests with the
    Basic authorization header. Therefore, we hard-code that
    check here.

    """
    @wraps(f)
    def decorated(*args, headers=None, **kwargs):
        bad_request = Response(status="401 Unauthorized")
        try:
            result = str(b64decode(headers['Basic']), 'utf-8')
            client_id, client_secret = result.split(":", 1)
            valid_login = all((
                client_id == current_app.config['BACKEND_ID'],
                client_secret == current_app.config['BACKEND_SECRET']
            ))
            if not valid_login:
                return bad_request
        except (KeyError, Error):
            return bad_request
        return f(*args, **kwargs)
    return decorated


class LDAPAuth(UserAuthnMethod):
    def get_template(self, template_name):
        """Load the login form template from source."""
        pkg_loader = PackageLoader('oidcp', 'templates')
        html = select_autoescape(['html'])
        env = Environment(loader=pkg_loader, autoescape=html)
        return env.get_template(template_name)

    def authenticated_as(self, cookie=None, **kwargs):
        """Determine if the cookie corresponds to an authenticated user.

        A key=state, value=cookie_hash pair is stored in Redis between the
        redirection that takes place for end-user authentication.

        When the end-user is redirected to the authorization endpoint and has
        successfully authenticated, there should be a key, value pair stored as
        state for the server to know that it can allow a code to be issued.
        The key, value pair is deleted once used. Once the flow is complete,
        the JWT token will control access.

        This behaviour is part of the in-house authentication implementation,
        as it is unspecified in the OpenID Connect specification[1].

        [1]: http://openid.net/specs/openid-connect-core-1_0.html#Authenticates

        """
        value = self.get_cookie_value(cookie, self.srv.cookie_name)
        if value is None:
            return None, 0

        state, timestamp, ctype = value

        return_code = self.srv.sdb._db.delete(state)
        if return_code == 0:
            return None, 0

        now = int(time.time())
        ttl = int(self.cookie_ttl * 60)
        if now > (int(timestamp) + ttl):
            raise ToOld("{} > ({} + {})".format(now, int(timestamp), ttl))

        return {"uid": state}, timestamp

    def __call__(self, *args, **kwargs):
        """Respond with the rendered login page.

        The rendering of the template threads the ``query`` parameter from the
        initial authorization request. This is a simple, stateless way to carry
        the initial request parameters through the flow.

        """
        template = self.get_template('login.html')
        endpoint = url_for('op.verify', _external=True)
        return Response(
            template.render({
                'endpoint': endpoint,
                'query': kwargs['query']
            })
        )

    def verify(self, request=None, **kwargs):
        """Validate the End-User credentials via LDAP."""
        ldap = current_app.config['LDAP_CONN']
        ldap_pattern = current_app.config['LDAP_PATTERN']
        ldap_url = current_app.config['LDAP_URL']
        ldap = ldap.initialize(ldap_url)

        uname = request['username']
        pword = request['password']

        try:
            query = parse_qs(request['query'])
        except KeyError:
            return Response(status='401 Unauthorized')

        try:
            uname_search = ldap_pattern.format(uname)
            ldap.simple_bind_s(uname_search, pword)
        except (INVALID_CREDENTIALS, TypeError):
            return Response(status='401 Unauthorized')

        state = query['state'][0]
        name = self.srv.cookie_name
        cookie_header = self.srv.cookie_func(state, "uam")
        cookie = SimpleCookie(cookie_header[1])
        self.srv.sdb[state] = cookie[name].value

        auth_url = url_for('op.auth', _external=True)
        return Redirect(
            redirect="{}?{}".format(auth_url, urlencode(query)),
            template=self.get_template('redirect.html'),
            headers=[cookie_header]
        )


def configure_auth_broker(provider):
    """Configure the authentication back-end.

    LDAP is our only authentication method.

    :param provider: The PyOIDC provider.

    """
    authn_broker = AuthnBroker()
    authn_broker.add("LDAP", LDAPAuth(provider))
    provider.authn_broker = authn_broker
    return provider
