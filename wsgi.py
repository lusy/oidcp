"""Bootstrap the application for web serving."""

from oidcp.app import create_app
from oidcp.config import Test

app = create_app(Test)
