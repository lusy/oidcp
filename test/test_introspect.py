# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""Introspection endpoint testing."""

from base64 import b64encode
from datetime import datetime as dt
from datetime import timedelta

import pytest
from flask import url_for
from freezegun import freeze_time
from werkzeug.datastructures import Headers


def test_introspect_post(client, token_params, session, auth_header):
    url = url_for("op.token")
    token_params.update({'code': session['code']})
    resp = client.post(url, data=token_params)
    assert resp.status_code == 200

    url = url_for("op.introspect")
    data = dict(
        access_token=resp.json['access_token'],
        token_type_hint='access_token',
    )

    resp = client.post(url, data=data, headers=auth_header)
    assert resp.status_code == 200
    assert resp.mimetype == "application/json"
    assert resp.json['active'] is True


def test_introspect_post_no_token(client, session, token_params, auth_header):
    url = url_for("op.introspect")
    data = dict(access_token='INVALID', token_type_hint='access_token')
    resp = client.post(url, data=data, headers=auth_header)
    assert resp.status_code == 400
    assert resp.mimetype == "text/html"


@pytest.mark.parametrize('params', (
    dict(token_type_hint='access_token'),
    dict(access_token='foo')
))
def test_introspect_post_bad_params(client, auth_header, params):
    url = url_for("op.introspect")
    for data in params:
        resp = client.post(url, data=data, headers=auth_header)
        assert resp.status_code == 400
        assert resp.mimetype == "text/html"


def test_introspect_post_bad_auth(client, token_params, session):
    url = url_for("op.token")
    token_params.update({'code': session['code']})
    resp = client.post(url, data=token_params)
    assert resp.status_code == 200

    url = url_for("op.introspect")
    token = resp.json['access_token']
    data = dict(access_token=token, token_type_hint='access_token')
    encoded = b64encode(bytes("IN:VALID", "utf-8")).decode("ascii")
    bad_header = Headers({'Basic': encoded})

    resp = client.post(url, data=data, headers=bad_header)
    assert resp.status_code == 401
    assert resp.mimetype == "text/html"


def test_introspect_post_expired(app, client, token_params,
                                 session, auth_header):
    url = url_for("op.token")
    token_params.update({'code': session['code']})
    resp = client.post(url, data=token_params)
    assert resp.status_code == 200

    url = url_for("op.introspect")
    data = dict(
        access_token=resp.json['access_token'],
        token_type_hint='access_token',
    )

    expired_token = dt.now() + timedelta(days=100)
    with freeze_time(expired_token):
        resp = client.post(url, data=data, headers=auth_header)
        assert resp.status_code == 200
        assert resp.mimetype == "application/json"
        assert resp.json['active'] is False


def test_introspect_post_revoked(app, client, token_params,
                                 session, auth_header):
    url = url_for("op.token")
    token_params.update({'code': session['code']})
    resp = client.post(url, data=token_params)
    assert resp.status_code == 200

    app.provider.sdb.revoke_token(session['code'])

    url = url_for("op.introspect")
    data = dict(
        access_token=resp.json['access_token'],
        token_type_hint='access_token',
    )

    resp = client.post(url, data=data, headers=auth_header)
    assert resp.status_code == 200
    assert resp.mimetype == "application/json"
    assert resp.json['active'] is False


def test_introspect_bad_http_method(client):
    url = url_for("op.introspect")
    resp = client.get(url)
    assert resp.status_code == 405
    assert resp.mimetype == "text/html"
