# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""The Application settings."""

import os

from oic.utils.authz import AuthzHandling
from oic.utils.client_management import pack_redirect_uri
from oidcp.session import SessionDB


class Base():
    """The base configuration."""
    APP_DIR = os.path.abspath(os.path.dirname(__file__))
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    FLOW = 'code'
    SCOPE = 'openid'
    ISSUER = 'OIDC:FSFE'
    FRONTEND_URI = 'https://ams.fsfe.org'
    JSON_AS_ASCII = False


class Prod(Base):
    """The production configuration."""
    from oidcp.session import RedisSessionDB
    from redis import Redis

    ENV = 'prod'
    DEBUG = False
    SESSION = SessionDB('', db=RedisSessionDB(Redis(
        host='localhost', port=6379, db=0
    )))


class Test(Base):
    """The test configuration."""
    from test.session import FakeRedisSessionDB
    from mockldap import MockLdap

    ENV = 'test'
    DEBUG = True
    SESSION = SessionDB('', db=FakeRedisSessionDB())
    AUTHZ = AuthzHandling()
    BACKEND_ID = 'backend_id'
    BACKEND_SECRET = 'backend_secret'
    FRONTEND_ID = 'frontend_id'
    CLIENT = {
        FRONTEND_ID: {
            'response_types_supported': 'code',
            'redirect_uris': pack_redirect_uri([Base.FRONTEND_URI]),
            'client_salt': 'frontend_salt',
            'client_secret': 'frontend_secret',
            'token_endpoint_auth_method': 'client_secret_post',
            'id_token_signed_response_alg': 'HS256'
        }
    }
    LDAP_URL = 'ldap://localhost/'
    LDAP_PATTERN = "cn={},ou=example,o=test"
    LDAP_CONN = MockLdap(dict([(
        'cn=alice,ou=example,o=test',
        {'cn': ['alice'], 'userPassword': ['alicepw']}
    )]))
