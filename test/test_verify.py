# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""Verify endpoint testing."""

from urllib.parse import urlencode

from flask import url_for

from http.cookies import SimpleCookie


def test_verify_post(app, auth_params, client, ldap_mock):
    url = url_for('op.verify')
    query = urlencode(auth_params)
    payload = dict(username='alice', password='alicepw', query=query)
    resp = client.post(url, data=payload)
    assert resp.status_code == 302
    assert resp.mimetype == 'text/html'

    name = app.provider.cookie_name
    cookie_func = app.provider.cookie_func

    actual_cookie = SimpleCookie(resp.headers['Set-Cookie'])
    actual_value = actual_cookie[name].value

    state = auth_params['state']
    expected_cookie = SimpleCookie(cookie_func(state, "uam")[1])
    expected_value = expected_cookie[name].value

    assert expected_value == actual_value

    assert app.provider.sdb._db[state] == actual_value


def test_verify_post_bad_login(client, auth_params, ldap_mock):
    url = url_for('op.verify')
    query = urlencode(auth_params)
    payload = dict(username='alice', password='INVALID', query=query)
    resp = client.post(url, data=payload)
    assert resp.status_code == 401
    assert resp.mimetype == 'text/html'


def test_verify_post_missing_query(client, auth_params, ldap_mock):
    url = url_for('op.verify')
    payload = dict(username='alice', password='INVALID')
    resp = client.post(url, data=payload)
    assert resp.status_code == 401


def test_verify_bad_http_method(client):
    url = url_for('op.verify')
    resp = client.get(url)
    assert resp.status_code == 405
    assert resp.mimetype == 'text/html'
