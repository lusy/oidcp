# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""The main application factory."""

from flask import Flask

from oidcp.auth import configure_auth_broker
from oidcp.config import Prod
from oidcp.provider import configure_provider
from oidcp.views import op_blueprint


def create_app(config=Prod):
    """Application factory.

    :param config: The configuration settings to use.
    """
    app = Flask(__name__.split('.')[0])
    app.config.from_object(config)
    app.register_blueprint(op_blueprint)
    provider = configure_provider(app)
    provider = configure_auth_broker(provider)
    app.provider = provider
    return app
