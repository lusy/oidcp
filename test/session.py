# oidcp: OpenId Connect Provider for the FSFE.
# Copyright (C) 2017 ams-hackers <ams-hackers@q.fsfe.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details <http://www.gnu.org/licenses/>.

"""A mocked Redis interface that does object pickling.

This is specifically for the testing environment. Please see the documentation
in ``oidcp.session`` for the rationale behind this interface.

"""

import pickle
from fakeredis import FakeRedis


class FakeRedisSessionDB(FakeRedis):
    """A Redis dict-like interface with object pickling."""
    def __getitem__(self, key):
        pickled = self.get(key)
        if pickled is None:
            raise KeyError()
        return pickle.loads(pickled)

    def __setitem__(self, key, val):
        pickled = pickle.dumps(val)
        self.set(key, pickled)
