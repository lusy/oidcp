[![build status](https://gitlab.com/lukewm/oidcp/badges/master/build.svg)](https://gitlab.com/lukewm/oidcp/commits/master)
[![coverage report](https://gitlab.com/lukewm/oidcp/badges/master/coverage.svg)](https://gitlab.com/lukewm/oidcp/commits/master)
[![Documentation Status](https://readthedocs.org/projects/oidcp/badge/?version=latest)](http://oidcp.readthedocs.io/en/latest/?badge=latest)

# oidcp

The FSFE's new OIDC provider.

```
$ python -m venv .venv  # create venv
$ . .venv/bin/activate  # activate it
$ pip install -U -r deps/dev.txt  # install deps
$ py.test  # run the tests
```

More coming soon™.
